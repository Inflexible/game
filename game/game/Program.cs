﻿using System;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 0; a < 2; a++)
            {

                Console.CursorVisible = false;

                int[,] field = new int[,]
                {
                    {1,1,1,1,1,1,1,1,1,1,1,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,0,0,0,0,0,0,0,0,0,0,1},
                    {1,1,1,1,1,1,1,1,1,1,1,1}
                };
                int[,] mineField = new int[10, 10];
                {
                    for (int i = 0; i < 10; i++) for (int j = 0; j < 10; j++) mineField[i, j] = 0;
                    for (int k = 1; k <= 10; k++)
                    {
                        int[] z;
                        var random = new Random();
                        z = new int[2] { random.Next(0, 9), random.Next(0, 9) };
                        if ((mineField[z[0], z[1]] != 0) || (z[0] + z[1] == 0) || (z[0] + z[1] == 18))
                            k--;
                        else
                        { mineField[z[0], z[1]] = random.Next(1, 10); }
                    }
                }

                int x = 1, y = 1;
                int hp = 10;
                while (x + y < 20 && hp > 0)
                {
                    Console.Clear();

                    for (int i = 0; i < field.GetLength(0); i++)

                    {
                        for (int j = 0; j < field.GetLength(1); j++)
                        {
                            if (field[i, j] == 0) Console.Write(".");
                            if (field[i, j] == 1) Console.Write("#");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine("Управление игроком производится стрелочками на клавиатуре");
                    Console.WriteLine("Здоровье игрока {0}/10", hp);

                    Console.CursorLeft = x;
                    Console.CursorTop = y;
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write("☺");
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.BackgroundColor = ConsoleColor.Black;

                    ConsoleKeyInfo ki = Console.ReadKey();
                    if (ki.Key == ConsoleKey.Escape) break;
                    if (ki.Key == ConsoleKey.LeftArrow && field[y, x - 1] == 0) x--;
                    if (ki.Key == ConsoleKey.RightArrow && field[y, x + 1] == 0) x++;
                    if (ki.Key == ConsoleKey.UpArrow && field[y - 1, x] == 0) y--;
                    if (ki.Key == ConsoleKey.DownArrow && field[y + 1, x] == 0) y++;

                    hp = hp - mineField[x - 1, y - 1];

                }
                Console.Clear();

                if (hp < 1)
                {
                    Console.WriteLine("Вы проиграли! Наступила смерть от ловушек");
                }

                if (x == 10 && y == 10)
                {
                    Console.WriteLine("Вы победили! Принцесса дождалась Вас :)");
                }
                if (a < 1)
                {
                    Console.WriteLine("Хотите запустить ещё одну сессию? y - да, n - нет");
                    string o = Console.ReadLine();
                    if (o!="y") { a++; }
                }
            }
        }
    }
}


